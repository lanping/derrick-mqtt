package com.logicalthinking.derrickmqtt.client;

import com.logicalthinking.derrick.utils.ConfigProperties;
import com.logicalthinking.derrickmqtt.utils.Constant;

public class TestSSLSubcribe {

    private static final String clientId = "drk-iot-server-01";

    public static void main(String[] args) {
        String host = ConfigProperties.getValue(Constant.CONFIG_KEY_MQTT_SERVER_URL);
        MQTTSubcribeClient mqttReceiver = new MQTTSubcribeClient(host, clientId);
    }
}

