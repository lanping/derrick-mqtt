package com.logicalthinking.derrickmqtt.dao;

import java.sql.Connection;

import com.logicalthinking.derrickmqtt.utils.DaoException;

/**
 * 事务处理
 * 
 * @author lanping
 * @version 1.0
 * @date 2020-01-04
 */
public interface ITransaction {

	/**
	 * 提交
	 */
	public void commit() throws DaoException;

	/**
	 * 回滚
	 */
	public void rollback() throws DaoException;

	/**
	 * 获取连接
	 */
	public Connection getConnection() throws DaoException;

	/**
	 * 关闭连接
	 */
	public void close() throws DaoException;

}