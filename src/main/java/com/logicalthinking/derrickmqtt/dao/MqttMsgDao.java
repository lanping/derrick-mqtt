package com.logicalthinking.derrickmqtt.dao;

import java.util.List;
import java.util.Map;

import com.logicalthinking.derrickmqtt.entity.po.MqttMsg;
import com.logicalthinking.derrickmqtt.utils.DaoException;

/**
 * mqtt遥测数据接口
 * @author lanping
 * @version 1.0
 * @date 2020-01-04
 */
public interface MqttMsgDao {

	/**
	 * 新增
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int insertMqttMsg(MqttMsg mqttMsg) throws DaoException;

	/**
	 * 新增
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int insertMqttMsg(MqttMsg mqttMsg, ITransaction tran)
			throws DaoException;

	/**
	 * 修改
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int updateMqttMsg(MqttMsg mqttMsg) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int updateMqttMsg(MqttMsg mqttMsg, ITransaction tran)
			throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteMqttMsg(Map<String, Object> map) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteMqttMsg(Map<String, Object> map, ITransaction tran)
			throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param mqttId
	 * @return
	 * @throws DaoException
	 */
	public MqttMsg selectMqttMsgByMqttId(String mqttId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<MqttMsg> selectMqttMsgList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<MqttMsg> selectAllMqttMsgList(Map<String, Object> map)
			throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectMqttMsgListCount(Map<String, Object> map) throws DaoException;

}