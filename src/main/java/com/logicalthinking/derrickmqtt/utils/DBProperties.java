package com.logicalthinking.derrickmqtt.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 读取DB配置文件
 * 
 * @author lanping
 * @version 1.0
 * @date 2020-01-04
 */
public class DBProperties extends Properties {

	private static final long serialVersionUID = 1L;
	private static DBProperties instance = null; // 这是对外提供的一个实例

	/**
	 * 外部调用这个方法来获取唯一的一个实例
	 */
	public synchronized static DBProperties getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new DBProperties();
			return instance;
		}
	}

	// 单例模式最核心的是构造方法私有化
	private DBProperties() {
		// 从db.properties文件中读取所有的配置信息
		InputStream is = this.getClass().getResourceAsStream("/db/db.properties");
		// 通过类的反射实例找到classpath路径下的资源文件,文件是db.properties,并建立一个流
		try {
			this.load(is); // 将流里面的字节码加载到MyPro对象中;
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}