package com.logicalthinking.derrickmqtt.entity;

/**
 * @author lanping
 * @version 1.0
 * @date 2019/12/30
 */
public class PayLoad<T> {

    private String msgType; //消息类型

    private String deviceType; //设备类型

    private String deviceName; //设备名称

    private Long ts;

    private T data;

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
