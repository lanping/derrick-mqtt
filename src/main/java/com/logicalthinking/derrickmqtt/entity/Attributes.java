package com.logicalthinking.derrickmqtt.entity;

/**
 * @author lanping
 * @version 1.0
 * @date 2019/12/30
 */
public class Attributes {

    private String type;
    private String skylineId;
    private String businessIds;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSkylineId() {
        return skylineId;
    }

    public void setSkylineId(String skylineId) {
        this.skylineId = skylineId;
    }

    public String getBusinessIds() {
        return businessIds;
    }

    public void setBusinessIds(String businessIds) {
        this.businessIds = businessIds;
    }
}
