package com.logicalthinking.derrickmqtt.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.logicalthinking.derrickmqtt.dao.BaseDao;
import com.logicalthinking.derrickmqtt.dao.ITransaction;
import com.logicalthinking.derrickmqtt.dao.MqttMsgDao;
import com.logicalthinking.derrickmqtt.entity.po.MqttMsg;
import com.logicalthinking.derrickmqtt.utils.DaoException;

import com.logicalthinking.derrickmqtt.utils.StringUtil;

/**
 * mqtt遥测数据实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2020-01-04
 */
public class MqttMsgDaoImpl extends BaseDao implements MqttMsgDao {

	/**
	 * 新增
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int insertMqttMsg(MqttMsg mqttMsg) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("insert into mqtt_msg ( ");
			if (StringUtil.isNotNull(mqttMsg.getMqttId())) {
				sb.append("mqtt_id , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getTopic())) {
				sb.append("topic , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getQos())) {
				sb.append("qos , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getJson())) {
				sb.append("json , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getRemark())) {
				sb.append("remark , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getState())) {
				sb.append("state , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getCreateTime())) {
				sb.append("create_time , ");
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) values ( ");
			if (StringUtil.isNotNull(mqttMsg.getMqttId())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getMqttId());
			}
			if (StringUtil.isNotNull(mqttMsg.getTopic())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getTopic());
			}
			if (StringUtil.isNotNull(mqttMsg.getQos())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getQos());
			}
			if (StringUtil.isNotNull(mqttMsg.getJson())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getJson());
			}
			if (StringUtil.isNotNull(mqttMsg.getRemark())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getRemark());
			}
			if (StringUtil.isNotNull(mqttMsg.getState())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getState());
			}
            if (StringUtil.isNotNull(mqttMsg.getCreateTime())) {
                sb.append(" ? , ");
                params.add(mqttMsg.getCreateTime());
            }
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) ");
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params);
		} catch (Exception e) {
			throw new DaoException(e,"新增MqttMsg时异常...");
		}
	}

	/**
	 * 新增
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int insertMqttMsg(MqttMsg mqttMsg, ITransaction tran) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("insert into mqtt_msg ( ");
			if (StringUtil.isNotNull(mqttMsg.getMqttId())) {
				sb.append("mqtt_id , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getTopic())) {
				sb.append("topic , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getQos())) {
				sb.append("qos , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getJson())) {
				sb.append("json , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getRemark())) {
				sb.append("remark , ");
			}
			if (StringUtil.isNotNull(mqttMsg.getState())) {
				sb.append("state , ");
			}
            if (StringUtil.isNotNull(mqttMsg.getCreateTime())) {
                sb.append("create_time , ");
            }
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) values ( ");
			if (StringUtil.isNotNull(mqttMsg.getMqttId())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getMqttId());
			}
			if (StringUtil.isNotNull(mqttMsg.getTopic())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getTopic());
			}
			if (StringUtil.isNotNull(mqttMsg.getQos())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getQos());
			}
			if (StringUtil.isNotNull(mqttMsg.getJson())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getJson());
			}
			if (StringUtil.isNotNull(mqttMsg.getRemark())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getRemark());
			}
			if (StringUtil.isNotNull(mqttMsg.getState())) {
				sb.append(" ? , ");
				params.add(mqttMsg.getState());
			}
            if (StringUtil.isNotNull(mqttMsg.getCreateTime())) {
                sb.append(" ? , ");
                params.add(mqttMsg.getCreateTime());
            }
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) ");
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"新增MqttMsg时异常...");
		}
	}

	/**
	 * 修改
	 * 
	 * @param MqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int updateMqttMsg(MqttMsg mqttMsg) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("update mqtt_msg set ");
			if (StringUtil.isNotNull(mqttMsg.getTopic())) {
				sb.append("topic = ? , ");
				params.add(mqttMsg.getTopic());
			}
			if (StringUtil.isNotNull(mqttMsg.getQos())) {
				sb.append("qos = ? , ");
				params.add(mqttMsg.getQos());
			}
			if (StringUtil.isNotNull(mqttMsg.getJson())) {
				sb.append("json = ? , ");
				params.add(mqttMsg.getJson());
			}
			if (StringUtil.isNotNull(mqttMsg.getRemark())) {
				sb.append("remark = ? , ");
				params.add(mqttMsg.getRemark());
			}
			if (StringUtil.isNotNull(mqttMsg.getState())) {
				sb.append("state = ? , ");
				params.add(mqttMsg.getState());
			}
            if (StringUtil.isNotNull(mqttMsg.getCreateTime())) {
                sb.append("create_time = ? , ");
                params.add(mqttMsg.getCreateTime());
            }
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" where mqtt_id = ? ");
			params.add(mqttMsg.getMqttId());
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params);
		} catch (Exception e) {
			throw new DaoException(e,"修改MqttMsg时异常...");
		}
	}

	/**
	 * 修改
	 * 
	 * @param mqttMsg
	 * @return
	 * @throws DaoException
	 */
	public int updateMqttMsg(MqttMsg mqttMsg, ITransaction tran) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("update mqtt_msg set ");
			if (StringUtil.isNotNull(mqttMsg.getTopic())) {
				sb.append("topic = ? , ");
				params.add(mqttMsg.getTopic());
			}
			if (StringUtil.isNotNull(mqttMsg.getQos())) {
				sb.append("qos = ? , ");
				params.add(mqttMsg.getQos());
			}
			if (StringUtil.isNotNull(mqttMsg.getJson())) {
				sb.append("json = ? , ");
				params.add(mqttMsg.getJson());
			}
			if (StringUtil.isNotNull(mqttMsg.getRemark())) {
				sb.append("remark = ? , ");
				params.add(mqttMsg.getRemark());
			}
			if (StringUtil.isNotNull(mqttMsg.getState())) {
				sb.append("state = ? , ");
				params.add(mqttMsg.getState());
			}
            if (StringUtil.isNotNull(mqttMsg.getCreateTime())) {
                sb.append("create_time = ? , ");
                params.add(mqttMsg.getCreateTime());
            }
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" where mqtt_id = ? ");
			params.add(mqttMsg.getMqttId());
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"修改MqttMsg时异常...");
		}
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteMqttMsg(Map<String, Object> map) throws DaoException {
		try {
			String sql = "delete from mqtt_msg where mqtt_id in ( "+map.get("mqttIds")+" ) ";
			System.out.println(sql);
			return singleUpdate(sql,null);
		} catch (Exception e) {
			throw new DaoException(e,"删除MqttMsg时异常...");
		}
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteMqttMsg(Map<String, Object> map, ITransaction tran) throws DaoException {
		try {
			String sql = "delete from mqtt_msg where mqtt_id in ( "+map.get("mqttIds")+" ) ";
			System.out.println(sql);
			return singleUpdate(sql, null, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"删除MqttMsg时异常...");
		}
	}

	/**
	 * 查询单个
	 * 
	 * @param mqttId
	 * @return
	 * @throws DaoException
	 */
	public MqttMsg selectMqttMsgByMqttId(String mqttId) throws DaoException {
		try {
			String sql="select * from mqtt_msg where mqtt_id = ? ";
			List<Object> params = new ArrayList<Object>();
			params.add(mqttId);
			System.out.println(sql);
			List<MqttMsg> list = findSingleTable(sql, params, MqttMsg.class);
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new DaoException(e,"查询单个MqttMsg时异常...");
		}
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<MqttMsg> selectMqttMsgList(Map<String, Object> map) throws DaoException {
		try {
			List<Object> params = new ArrayList<Object>();
			String sql = "select * from mqtt_msg where 1=1 ";
			if (map.get("mqttId") != null) {
				sql += " and mqtt_id =? ";
				params.add(map.get("mqttId"));
			}
			if (map.get("topic") != null) {
				sql += " and topic =? ";
				params.add(map.get("topic"));
			}
			if (map.get("qos") != null) {
				sql += " and qos =? ";
				params.add(map.get("qos"));
			}
			if (map.get("json") != null) {
				sql += " and json =? ";
				params.add(map.get("json"));
			}
			if (map.get("remark") != null) {
				sql += " and remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
            if (map.get("createTime") != null) {
                sql += " and create_time =? ";
                params.add(map.get("createTime"));
            }
			if (map.get("sortName") != null) {
				sql += " order by "+ map.get("sortName");
			}
			if (map.get("sortOrder") != null) {
				sql += " "+map.get("sortOrder") ;
			}
			if(map!=null && StringUtil.isNotNull(map.get("pageIndex")) && StringUtil.isNotNull(map.get("pageSize"))){
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				sql+=" LIMIT "+count+","+size;
			}
			System.out.println(sql);
			return findSingleTable(sql, params, MqttMsg.class);
		} catch (Exception e) {
			throw new DaoException(e,"分页查询MqttMsg时异常...");
		}
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<MqttMsg> selectAllMqttMsgList(Map<String, Object> map) throws DaoException {
		try {
			String sql = "select * from mqtt_msg where 1=1 ";
			List<Object> params = new ArrayList<Object>();
			if (map.get("mqttId") != null) {
				sql += " and mqtt_id =? ";
				params.add(map.get("mqttId"));
			}
			if (map.get("topic") != null) {
				sql += " and topic =? ";
				params.add(map.get("topic"));
			}
			if (map.get("qos") != null) {
				sql += " and qos =? ";
				params.add(map.get("qos"));
			}
			if (map.get("json") != null) {
				sql += " and json =? ";
				params.add(map.get("json"));
			}
			if (map.get("remark") != null) {
				sql += " and remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
            if (map.get("createTime") != null) {
                sql += " and create_time =? ";
                params.add(map.get("createTime"));
            }
			if (map.get("sortName") != null) {
				sql += " order by "+ map.get("sortName");
			}
			if (map.get("sortOrder") != null) {
				sql += " "+map.get("sortOrder") ;
			}
			System.out.println(sql);
			return findSingleTable(sql, params, MqttMsg.class);
		} catch (Exception e) {
			throw new DaoException(e,"查询所有MqttMsg时异常...");
		}
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectMqttMsgListCount(Map<String, Object> map) throws DaoException {
		try {
			String sql = "select count(1) from mqtt_msg where 1=1 ";
			List<Object> params = new ArrayList<Object>();
			if (map.get("mqttId") != null) {
				sql += " and mqtt_id =? ";
				params.add(map.get("mqttId"));
			}
			if (map.get("topic") != null) {
				sql += " and topic =? ";
				params.add(map.get("topic"));
			}
			if (map.get("qos") != null) {
				sql += " and qos =? ";
				params.add(map.get("qos"));
			}
			if (map.get("json") != null) {
				sql += " and json =? ";
				params.add(map.get("json"));
			}
			if (map.get("remark") != null) {
				sql += " and remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
            if (map.get("createTime") != null) {
                sql += " and create_time =? ";
                params.add(map.get("createTime"));
            }
			System.out.println(sql);
			List<String> list = uniqueResult(sql, params);
			if (list != null && list.size() > 0) {
				return Integer.parseInt(list.get(0).toString());
			}
			return 0;
		} catch (Exception e) {
			throw new DaoException(e,"查询MqttMsg总数时异常...");
		}
	}

}