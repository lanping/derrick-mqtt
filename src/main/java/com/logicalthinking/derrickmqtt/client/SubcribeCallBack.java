package com.logicalthinking.derrickmqtt.client;

import com.logicalthinking.derrickmqtt.dao.MqttMsgDao;
import com.logicalthinking.derrickmqtt.dao.impl.MqttMsgDaoImpl;
import com.logicalthinking.derrickmqtt.entity.po.MqttMsg;
import com.logicalthinking.derrickmqtt.utils.Constant;
import com.logicalthinking.derrickmqtt.utils.DaoException;
import com.logicalthinking.derrickmqtt.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.UUID;

@Slf4j
public class SubcribeCallBack implements MqttCallbackExtended {

    @Override
    public void connectionLost(Throwable throwable) {
        log.warn("client lost connection,reconnecting");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

        try {
            // subscribe后得到的消息会执行到这里面
            String content = new String(mqttMessage.getPayload());
            log.info("接收消息主题 : " + topic);
            log.info("接收消息Qos : " + mqttMessage.getQos());
            log.info("接收消息内容 : " + content);
            String str = mqttMessage.toString();
            log.info("从MQTT收到的消息为:" + str + " ;TOPIC:" + topic);

            writeDB(content,topic,mqttMessage.getQos());
        } catch (Exception e) {
            log.error("SubcribeCallBack error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void writeDB(String content,String topic,int qos) throws DaoException {
        //写入数据库
        MqttMsgDao mqttMsgDao = new MqttMsgDaoImpl();
        MqttMsg mqttMsg = new MqttMsg();
        mqttMsg.setMqttId(UUID.randomUUID().toString());
        mqttMsg.setJson(content);
        mqttMsg.setQos(qos);
        mqttMsg.setTopic(topic);
        mqttMsg.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
        mqttMsg.setState(Constant.IS_VALID_Y);
        mqttMsgDao.insertMqttMsg(mqttMsg);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        log.info("deliveryComplete---------" + iMqttDeliveryToken.isComplete());
    }

    @Override
    public void connectComplete(boolean b, String s) {
        log.info("receive connectted");
    }

}

