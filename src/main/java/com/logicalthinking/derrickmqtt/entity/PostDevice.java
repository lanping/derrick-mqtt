package com.logicalthinking.derrickmqtt.entity;

public class PostDevice {

    private TenantId tenantId;
    private String type;
    private Originator originator;
    private String severity;
    private String status;
    private long startTs;
    private long endTs;
    private int ackTs;
    private int clearTs;
    private Details details;
    private boolean propagate;
    private Id id;
    private long createdTime;
    private String name;

    public void setTenantId(TenantId tenantId) {
        this.tenantId = tenantId;
    }

    public TenantId getTenantId() {
        return tenantId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setOriginator(Originator originator) {
        this.originator = originator;
    }

    public Originator getOriginator() {
        return originator;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getSeverity() {
        return severity;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStartTs(long startTs) {
        this.startTs = startTs;
    }

    public long getStartTs() {
        return startTs;
    }

    public void setEndTs(long endTs) {
        this.endTs = endTs;
    }

    public long getEndTs() {
        return endTs;
    }

    public void setAckTs(int ackTs) {
        this.ackTs = ackTs;
    }

    public int getAckTs() {
        return ackTs;
    }

    public void setClearTs(int clearTs) {
        this.clearTs = clearTs;
    }

    public int getClearTs() {
        return clearTs;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public Details getDetails() {
        return details;
    }

    public void setPropagate(boolean propagate) {
        this.propagate = propagate;
    }

    public boolean getPropagate() {
        return propagate;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Id getId() {
        return id;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}