package com.logicalthinking.derrickmqtt.entity.po;

/**
 * mqtt遥测数据
 * 
 * @author lanping
 * @version 1.0
 * @date 2020-01-04
 */
public class MqttMsg { 

	private String mqttId;	//主键ID
	private String topic;	//主题
	private Integer qos;	//qos
	private String json;	//数据
	private String remark;	//备注
	private String createTime;	//创建时间
	private String state;	//状态Y有效N无效

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getMqttId() {
		return mqttId;
	}

	public void setMqttId(String mqttId) { 
		this.mqttId = mqttId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) { 
		this.topic = topic;
	}

	public Integer getQos() {
		return qos;
	}

	public void setQos(Integer qos) { 
		this.qos = qos;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) { 
		this.json = json;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "MqttMsg [mqttId=" + mqttId + ",topic=" + topic + ",qos=" + qos + ",json=" + json + ",remark=" + remark + ",state=" + state + "]";
	}
}