package com.logicalthinking.derrickmqtt.entity;

public class Msg {

    private boolean active;
    private long lastConnectTime;
    private int lastActivityTime;
    private int lastDisconnectTime;
    private long lastInactivityAlarmTime;
    private int inactivityTimeout;

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getActive() {
        return active;
    }

    public void setLastConnectTime(long lastConnectTime) {
        this.lastConnectTime = lastConnectTime;
    }

    public long getLastConnectTime() {
        return lastConnectTime;
    }

    public void setLastActivityTime(int lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    public int getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastDisconnectTime(int lastDisconnectTime) {
        this.lastDisconnectTime = lastDisconnectTime;
    }

    public int getLastDisconnectTime() {
        return lastDisconnectTime;
    }

    public void setLastInactivityAlarmTime(long lastInactivityAlarmTime) {
        this.lastInactivityAlarmTime = lastInactivityAlarmTime;
    }

    public long getLastInactivityAlarmTime() {
        return lastInactivityAlarmTime;
    }

    public void setInactivityTimeout(int inactivityTimeout) {
        this.inactivityTimeout = inactivityTimeout;
    }

    public int getInactivityTimeout() {
        return inactivityTimeout;
    }

}