package com.logicalthinking.derrickmqtt.utils;

/**
 * @author lanping
 * @version 1.0
 * @date 2019/12/30
 */
public class Constant {

    /**
     * Y
     */
    public static final String IS_VALID_Y="Y";

    /**
     * N
     */
    public static final String IS_VALID_N="N";

    /**
     * mqtt服务器地址
     */
    public static final String CONFIG_KEY_MQTT_SERVER_URL="MQTT_SERVER_URL";

    /**
     * CLIENT_KEY_PASS
     */
    public static final String CONFIG_KEY_CLIENT_KEY_PASS="CLIENT_KEY_PASS";

    /**
     * CRT_ROOT_PATH
     */
    public static final String CONFIG_KEY_CRT_ROOT_PATH="CRT_ROOT_PATH";

    public static String [] topics={
            "iot/event/ENTITY_CREATED","iot/event/DISCONNECT_EVENT","iot/event/INACTIVITY_EVENT",
            "iot/event/ACTIVITY_EVENT", "iot/event/ENTITY_DELETED",
            "iot/event/POST_ATTRIBUTES_REQUEST","iot/event/POST_TELEMETRY_REQUEST"
    };

    public static int [] qos = {1,1,1,1,1,1,1};
}
