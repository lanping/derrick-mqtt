package com.logicalthinking.derrickmqtt.entity;

public class Originator {

    private String entityType;
    private String id;

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}