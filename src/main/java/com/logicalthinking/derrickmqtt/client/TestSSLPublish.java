package com.logicalthinking.derrickmqtt.client;

import com.alibaba.fastjson.JSON;
import com.logicalthinking.derrick.utils.ConfigProperties;
import com.logicalthinking.derrickmqtt.entity.Attributes;
import com.logicalthinking.derrickmqtt.entity.PayLoad;
import com.logicalthinking.derrickmqtt.utils.Constant;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class TestSSLPublish {

    public static void main(String[] args) throws Exception {

        String host = ConfigProperties.getValue(Constant.CONFIG_KEY_MQTT_SERVER_URL);
        String serverId = String.valueOf(Math.random());
        //mqtt发送端
        MQTTPublishClient mqttClientSend = new MQTTPublishClient(host, serverId);
        MqttMessage message = new MqttMessage();
        String topic = "iot/event/POST_ATTRIBUTES_REQUEST";

        PayLoad<Attributes> payLoad = new PayLoad<>();
        payLoad.setMsgType("POST_ATTRIBUTES_REQUEST");
        payLoad.setDeviceName("111");
        payLoad.setDeviceType("111");
        Attributes attributes = new Attributes();
        attributes.setType("1");
        attributes.setBusinessIds("['7797dee07ae8f8201b0faed5b1a4beb3', 'b75d9f78faa176ec56fb89b3c1e4cdb7']");
        attributes.setSkylineId("1");
        payLoad.setData(attributes);

        message.setPayload(JSON.toJSONString(payLoad).getBytes("UTF-8"));
        message.setQos(1);
        message.setRetained(false);
        System.out.println("MQTT发送消息");
        mqttClientSend.publish(topic, message);
    }
}

